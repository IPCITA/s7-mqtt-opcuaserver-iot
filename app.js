/* global require */
var async = require('asyncawait/async');
var await = require('asyncawait/await');

//s7 connection import
var S7Conn = require('./modules/s7-driver.js');
//read config file
const appConfig = require("./config/app-config.json")
//opcua server import
var OPCUAServer = require('./modules/opcua-server.js');

//cyclic function that read from all S7Clients and publish data to MQTT
var readPolling = async (function (S7Clients, appConfig)
{
	//acquisition time [ms] from S7 devices
	var cycleTime = appConfig.serverConfig.cycleTime;

	//re-schedule execution at pollingtime [ms]
	setTimeout (readPolling, cycleTime, S7Clients, appConfig);

	//for each client read variable and push to broker
	for (let i = 0; i < S7Clients.length; i++)
	{
		//store some values
		var deviceConfig = appConfig.S7Devices[i].deviceConfig;
		var dataSet = S7Clients[i].dataSet;

		try
			{
				//read S7 variables response array 
				S7Conn.S7ReadClients (S7Clients[i], deviceConfig, dataSet);
				//console.log(readData);
				//console.log("Response from S7 Clients at", new Date());
			}
			catch (e)
			{
				console.log (e);
			}
	}
});
//initialization of all S7 connections, MQTT data exchange and OPCUA Server
var Start = async (function ()
{
				try
				{
					//Initialize all S7Clients connections and their mqtt modules
					var S7Clients = await (S7Conn.S7Initialize(appConfig));
					console.log ('S7 connection initalized...');
					
					//loop read and publishing to mqtt broker
					console.log ('Start S7 Pub Polling...');
					readPolling (S7Clients, appConfig);					
					
					//Create OPC UA Server and connect it to mqtt broker
					console.log ('Start OPCUA Server...');
					var OPCServer = await (OPCUAServer.startAsync (appConfig));
				}
				catch (e)
				{
				console.log (e);
				}
});

//________MAIN________//
Start();
