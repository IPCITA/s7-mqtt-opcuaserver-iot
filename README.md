# s7-mqtt-opcuaserver-iot
![enter image description here](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/raw/master/docs/app-workflow.png)
### A simple example of OPC UA Server that interacts with SIEMENS S7 Devices

Example of Node.JS v6 based Application that easy map variables from different SIEMENS S7 devices to a Server based on OPC UA protocol, 
thus making available the various data of the S7 controllers for reading / writing via the OPC UA connected Clients.

* Easy Install with Configuration and installation script
* Multiple connection of different SIMATIC PLCs and other SIEMENS S7 devices
* OPC UA Server with "Sign & Encrypt" option and integrated user authentication
* Bi-directional communication S7 - OPCUA Server for writing  and reading variables.

## Supported only iot2000 example image v2.2!

## For ITALIAN user [README_ITA.md](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/docs/README_ITA.md)
## Table of Contents

-   [Before Starting](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/README.md#before-starting)
-   [Getting Started](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/README.md#getting-started)
-   [First Configuration](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/README.md#first-configuration)
-   [App Configuration](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/README.md#app-configuration)
    -   [OPC UA Server Configuration](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/README.md#opcu-ua-server.configuration)
    -   [S7 Devices Configuration](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/README.md#s7-devices-configuration)
-   [Run the application](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/README.md#run-the-application)
-   [Manage Startup and Restart with PM2 App](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/README.md#manage-startup-and-restart-with-pm2-app)
-   [Connecting to Server OPC UA ](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/README.md#connecting-to-server-OPC-UA )
-   [Installation on other systems](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/README.md#installation-on-other-systems) 
-   [Configuration Script](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/README.md#configuration-script)
-   [Install Git on SIMATIC IOT2040](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/README.md#install-GIT-on-SIMATIC-IOT2040)
-   [References](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/README.md#references)
-   [Release History](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/README.md#release-history)
-   [Contributing](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/README.md#contributing)
-   [License](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/README.md#license)
-   [Contact](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/README.md#contact)

## Before Starting

This guide makes you  successfully install the *s7-mqtt-opcuaserver-iot* app. To get started, see the chapter **Getting Started**.

The application comes pre-compiled for SIMATIC IOT2040. For using it with other platforms, see the chapter **Installation on other systems**.

The application uses *Mosquitto MQTT Broker* (pre-installed in SIMATIC IOT2040) as an internal data exchange bus. 
In order to run the application correctly, Mosquitto application must be configured. The script _ **./configure.sh** is supplied with 
some necessary steps for the configuration automatically performed. For more information see the chapter **First Configuration**.

Before using the application it is necessary to configure the options of *OPC UA Server* and all neeeded parameters of every *SIEMENS S7* devices 
which you want to connect. Configuration is possible via the ***app-config.json*** app. For more information, see the chapter **App Configuration**.

Thanks to **PM2** the execution of this application can be automatically managed. With PM2 at every new system boot the *s7-mqtt-opcuaserver-iot* 
application is started and in case of errors it is automatically restarted to allow operation continuity.
Script ***./install-pm2.sh*** is supplied with which the necessary steps for the installation and configuration of PM2 are automatically performed. 
For more information, see the chapter **Manage Startup and Restart with PM2 App**.


## Getting Started

Open a terminal and go to */opt* folder (or to the desired folder). Clone the directory of the *s7-mqtt-opcuaserver-iot* app from the GitLab repository
(To install the *git* command on IOT2040 see paragraph **Install Git on SIMATIC IOT2040**).

```
cd /opt
git clone https://gitlab.com/IPCITA/s7-mqtt-opcuaserver-iot.git
```

or download it as a compressed file, unzip it and copy it to the */opt* folder (or to the desired folder).

All *node_modules* are pre-compiled for IOT2040. so it is not necessary to run the npm install command to install the Node.JS modules. 
For use with other platforms, see the chapter **Installation on other systems**.


## First Configuration

The script ***./configure.sh*** is supplied with some of the steps necessary for configuration which are automatically performed:

 - copy of the correct configuration files in the Mosquitto installation folder
 - generation of files for CA Security Certificate with hostname and automatic IP address

To start the script, go to the device in the app folder and start the script

```
cd /opt/s7-mqtt-opcuaserver-iot
chmod +x ./configure.sh
./configure.sh
```

During the execution of the script will be shown information on the various steps taken and on the subsequent actions to be taken. 
Below is an example of ***output*** of the execution of *./configure.sh* script:

```
root@iot2040:~ cd /opt/s7-mqtt-opcuaserver-iot
root@iot2040:~/s7-mqtt-opcuaserver-iot# chmod +x ./configure.sh
root@iot2040:~/s7-mqtt-opcuaserver-iot# ./configure.sh
######################################################################
############# Copying Mosquitto MQTT configuration files #############
cp ./mqtt/mosquitto.conf /etc/mosquitto
cp ./mqtt/aclfile /etc/mosquitto
cp ./mqtt/auth /etc/mosquitto
######################################################################
####################### Generate CA Certificates #####################
############## Creating your new certificates. Wait! #################
your hostname is iot2040
Find eth0 with IP Address 192.168.200.1
Find eth1 with IP Address 192.168.1.180
OpenSSL config generated.
Generating a 2048 bit RSA private key
............+++
....+++
writing new private key to './cert/private_key.pem'
-----
######################################################################
###################### CONFIGURATION COMPLETED! ######################
######################################################################
### SET AUTOSTART OF MOSQUITTO SERVICE USING iot2000setup COMMAND. ###
############ GO TO Software -> Manage Autostart Options AND ##########
############ ENABLE Autostart Mosquitto Broker.'            ##########
######################################################################
##### AFTER SETTINGS ABOVE REBOOT YOUR SYSTEM TO APPLY SETTINGS! #####
############### RUN install.sh AFTER REBOOT FOR SET       ############
############### AUTOSTART s7-mqtt-opcuaserver-iot AT BOOT.############
######################################################################
```

For more details about the operations performed by the *./configure.sh* script, see the **Configuration Script** chapter.

Before starting the application it is necessary to enable ***automatic startup*** at boot of service *Mosquitto MQTT Broker*.
Through integrated command ***iot2000setup*** on SIMATIC IOT2040 this operation can be done easily:

```
iot2000setup
```

![enter image description here](https://hackster.imgix.net/uploads/attachments/405371/Screenshot_20171222_143951.png?auto=compress,format&w=500&h=350)

In menu ***Software*** -> *Manage Autostart Options* is only necessary enable *Autostart Mosquitto Broker* option.
At the end of the described operations restart the device:

```
reboot
```

## App Configuration

To change OPC UA Server parameters, use file ***./config/app-config.json***.This file contains all the parameters necessary for configuring the application.
In the first part of the file there are two parameters for information purposes on the application:
```
{
"appName": "s7-mqtt-opcuaserver-iot",
"description": "Sample configuration file for two PLC S7-1500",
......................
```
Then the configuration is divided into two main parts, ***serverConfig*** and ***S7Devices***.

### OPC UA Server Configuration
In the ***"serverConfig"*** object property you can modify the port on which the OPC Server UA can be reached (parameter ***"port"*** ) and also
the minimum sampling time [ms] of the variables read by the SIEMENS S7 devices with the parameter ***"cycleTime"*** .
Optionally, by settings the parameter ***"allowAnonymous"*** you can enable or disable user authentication on OPC UA Server with custom users from ***"userList"*** array property. 

> If ***"allowAnonymous"*** is set to *false* it will not be possible to access the Server without configured user and password entered by the OPC UA Client.
 
> With SIMATIC IOT2040, parameter ***"cycleTime"*** should never be set below 500 ms for a large number of tags or connections.

```
	................................ 
	"serverConfig" : 
	{
			"cycleTime" : 500,
			"port" : 4840,
			"certificateFile": "./cert/certificate.pem",
			"privateKeyFile": "./cert/private_key.pem",
			"allowAnonymous" : false,
			"userList" : 
			[
				{
					"user" : "Siemens" ,
					"password" : "Siemens"
				},
				{
					"user" : "user" ,
					"password" : "user"
				}
			]
	},
	.............................
```

### S7 Devices Configuration

In the second part of the configuration we find the array ***S7Devices***, which contains the configuration of the different SIEMENS S7 controllers and their variables.
Each element of the array is an object that represents all the information associated with a single SIEMENS S7 device.

The parameter **"deviceName"** indicates the name of the S7 device and will be the corresponding name of the object "Folder" of the OPC UA Server.
It is also possible to enter a description of the device for information purposes.

> ***Note:*** The parameter ***"deviceName"*** MUST NOT contain the character *"/"* as it is typical of the MQTT protocol. Use may lead to malfunctions of the MQTT Data Bus.

Below you will find an **example** of configuration *S7Devices* with the use of two PLCs *SIEMENS S7-1500*. 
Then all the details on the meaning of the various parameters are indicated.

```
    ...............................
    "S7Devices" : 
	[
		{
			"deviceName" : "PLC_1",
			"description" : "S7-1500 PLC_1 TEST",
			"deviceConfig" : 
			{
					"ipAddress" : "192.168.200.10",
					"rack" : 0,
					"slot" : 1
			},
			"variablesList": 
			[
				{
						"name": "Tag0",
						"Area" : "DB",
						"DBNumber": 1,
						"offset": 6,
						"dataType": "Int",
						"writable": false
				},
				{
						"name": "Tag1",
						"Area" : "DB",
						"DBNumber": 1,
						"offset": 8,
						"dataType": "Int",
						"writable": false
				},
				{
						"name": "Tag2",
						"Area" : "DB",
						"DBNumber": 1,
						"offset": 10,
						"dataType": "Int",
						"writable": false
				}
				..........and other tags........
			]
		},
		{
			"deviceName" : "PLC_2",
			"description" : "S7-1500 PLC_2 TEST",
			"deviceConfig" : 
			{
					"ipAddress" : "192.168.200.10",
					"rack" : 0,
					"slot" : 1
			},
			"variablesList": 
			[
				{
						"name": "Tag13",
						"Area" : "DB",
						"DBNumber": 1,
						"offset": 36,
						"stringLen" : 254,
						"dataType": "String",
						"writable": true
				},
				{
						"name": "Tag14",
						"Area" : "DB",
						"DBNumber": 1,
						"offset": 292,
						"index" : 0,
						"dataType": "Bool",
						"writable": true
				}
				..........and other tags........
			]
		}
		..........and other S7 Devices........
	]	 
}            
```

In **"deviceConfig"** the connection parameters of each SIEMENS S7 device are indicated. 
Insert here the correct values of **"ipAddress"**, **"rack"** and **"slot"** of the single device.

| S7 Model | Rack | Slots |
| ------ | ------ | ------ |
| S7-1500, S7-1200, ... | 0 | 1 |
| S7-300, LOGO !, ... | 0 | 2 |

The **"variablesList"** property is an array that lists all the desired variables to be connected to the OPC UA Server and their properties.

> **Note:** In order to properly operate S7 communication with some new generation SIEMENS devices (such as S7-1500, S7-1200) it is necessary to modify CPU parameters using the SIEMENS TIA Portal software before being able to read or write variables. Specifically, the S7 device must meet the following requirements:
> - option ***"Allow access via PUT / GET communication with remote partners"*** enabled;
> - expose only Data Blocks (DB) with ***"Not Optimized Access"*** .
> **Warning:** activation of this functionality lower PLCs security protection!


The parameter **"name"** indicates the name of the variable connected within the OPC UA Server.
It is possible to read and write different *Memory Areas* of S7 devices, specifying the parameter **"Area"**:

| Area | Description |
| ------ | ------ |
| MK | Merkers |
| DB | DB |
| PE | Process inputs |
| PA | Process outputs |
| CT | Counters |
| TM | Timers |

To specify which variables can *only* be read by the S7 device and which can be *read and written* by the OPC UA Server the ***"writable"*** property is used as boolean.

For each variable it is necessary to set the type of the S7 data using the parameter **"dataType"**.
Actual supported datatypes are ***'Real'*** , ***'Byte'*** , ***'Word'*** , ***'DWord'*** , ***'Int'*** , ***'UInt'*** , ***'DInt'*** , ***'UDInt'*** , ***'Bool'*** ,
***'String'*** respect to STEP7 language model.

> ***Note:*** Respect uppercase and lowercase, datatype names are the same as those used by TIA Portal!

With the parameter **"offset"** the Byte of the relative address of the variable is specified. 
This column in the TIA Portal is only visible in data blocks with non-optimized standard access.
(*eg* S7 variable of type 'Word' with *Offset TIA = 6.0* must be specified with **offset = 6**).

![enter image description here](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/raw/master/docs/DB_IOT_Tags.PNG)

For ***'Bool'*** type variables, the **"index"** parameter must also be set as the Bit of the variable's memory address.
(*eg* A S7 variable of type 'Bool' with *TIA Offset  = 10.1* must be specified with **index = 1** )

For  ***'String'*** variables it is necessary to set the size of the string in Bytes on the property **"stringLen"** (max 254 Byte).

## Run the Application

Once the configuration has been performed using the previous steps, it is possible to start the application manually using the following commands from the terminal:

```
cd /opt/s7-mqtt-opcuaserver-iot
node app.js
```
Below is an example of the output of the program *s7-mqtt-opcuaserver-iot* in operation:

```
root @ iot2040: ~# cd /opt/s7-mqtt-opcuaserver-iot/
root @ iot2040: ~ /s7-mqtt-opcuaserver-iot # node app.js
S7 connection initalized ...
Start S7 Pub Polling ...
Start OPCUA Server ...
The primary Server endpoint URL is opc.tcp://192.168.200.1:4840/ua/node-opcua
OPCUA Server started.
OPCUA Server is now listening on port 4840 (press CTRL + C to stop Server).
```
After startup of app, the OPC UA Server address can be checked in the log on console as parameter ***endpoint URL*** . 
Copy the address and keep it for the new OPC UA clients.

```
..... '
The primary Server endpoint URL is opc.tcp://192.168.200.1:4840/ua/node-opcua
.....
```

## Manage Startup and Restart with PM2 App

PM2 tool can automatically manage the execution of this application. 
With PM2 on every new system boot the s7-mqtt-opcuaserver-iot application is started and in case of errors it is automatically restarted to allow operation continuity.

The script ***./install-pm2.sh*** is supplied with which the tool ***PM2*** is automatically installed and the necessary steps are taken to configure it.
To start the script go to the device in the app folder and start the script:

```
cd /opt/s7-mqtt-opcuaserver-iot
chmod +x ./install.sh
./install.sh
```
Below is an example of ***output*** of running *./install.sh*:

```
root@iot2040:~ cd /opt/s7-mqtt-opcuaserver-iot
root@iot2040:~/s7-mqtt-opcuaserver-iot# chmod +x ./install.sh
root@iot2040:~/s7-mqtt-opcuaserver-iot# ./install-pm2.sh
######################################################################
################# Installing PM2 for Managing Apps! ##################
################## Check your internet connection! ###################
/usr/bin/pm2 -> /usr/lib/node_modules/pm2/bin/pm2
/usr/bin/pm2-dev -> /usr/lib/node_modules/pm2/bin/pm2-dev
/usr/bin/pm2-docker -> /usr/lib/node_modules/pm2/bin/pm2-docker
/usr/bin/pm2-runtime -> /usr/lib/node_modules/pm2/bin/pm2-runtime
/usr/lib
`-- pm2@3.5.0
  `-- shelljs@0.8.3
    `-- glob@7.1.4

npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@^1.2.7 (node_modules/pm2/node_modules/chokidar/node_modules/fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.9: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"ia32"})
######################################################################
################# Creating startup script of PM2 #####################
[PM2] Init System found: upstart
Platform upstart
Template
#!/bin/bash
### BEGIN INIT INFO
# Provides:        pm2
# Required-Start:  $local_fs $remote_fs $network
# Required-Stop:   $local_fs $remote_fs $network
# Default-Start:   2 3 4 5
# Default-Stop:    0 1 6
# Short-Description: PM2 Init script
# Description: PM2 process manager
### END INIT INFO

NAME=pm2
PM2=/usr/lib/node_modules/pm2/bin/pm2
USER=root
DEFAULT=/etc/default/$NAME

export PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin:$PATH
export PM2_HOME="/home/root/.pm2"

...............
...............installation output.....................
...............
............................................................

 System startup links for /etc/init.d/pm2-root already exist.
[PM2] [v] Command successfully executed.
+---------------------------------------+
[PM2] Freeze a process list on reboot via:
$ pm2 save

[PM2] Remove init script via:
$ pm2 unstartup upstart
######################################################################
############### Running application with PM2 System ##################
[PM2] Starting /opt/s7-mqtt-opcuaserver-iot/app.js in fork_mode (1 instance)
[PM2] Done.
┌─────────────────────┬────┬─────────┬──────┬──────┬────────┬─────────┬────────┬─────┬──────────┬──────┬──────────┐
│ App name            │ id │ version │ mode │ pid  │ status │ restart │ uptime │ cpu │ mem      │ user │ watching │
├─────────────────────┼────┼─────────┼──────┼──────┼────────┼─────────┼────────┼─────┼──────────┼──────┼──────────┤
│ s7-mqtt-opcuaserver │ 0  │ 1.0.0   │ fork │ 2983 │ online │ 0       │ 0s     │ 0%  │ 9.8 MB   │ root │ disabled │
└─────────────────────┴────┴─────────┴──────┴──────┴────────┴─────────┴────────┴─────┴──────────┴──────┴──────────┘
 Use `pm2 show <id|name>` to get more details about an app
######################################################################
##################### Save actual changes in PM2 #####################
[PM2] Saving current process list...
[PM2] Successfully saved in /home/root/.pm2/dump.pm2
######################################################################
###################### INSTALLATION COMPLETED! #######################
######################################################################
##################### REBOOT FOR APPLY CHANGES! ######################
######################################################################

```
Restart the system will apply all changes above and the application will be running automatically.

Some ***PM2*** functions can be used to check the status of the application, like the two below:

*  ### pm2 list

Show all applications managed by PM2 in a table.
```
root@iot2040:~# pm2 list
┌─────────────────────┬────┬─────────┬──────┬──────┬────────┬─────────┬────────┬─────┬──────────┬──────┬──────────┐
│ App name            │ id │ version │ mode │ pid  │ status │ restart │ uptime │ cpu │ mem      │ user │ watching │
├─────────────────────┼────┼─────────┼──────┼──────┼────────┼─────────┼────────┼─────┼──────────┼──────┼──────────┤
│ s7-mqtt-opcuaserver │ 0  │ 1.0.0   │ fork │ 8768 │ online │ 0       │ 4m     │ 43% │ 69.7 MB  │ root │ disabled │
└─────────────────────┴────┴─────────┴──────┴──────┴────────┴─────────┴────────┴─────┴──────────┴──────┴──────────┘

 Use `pm2 show <id|name>` to get more details about an app
```
 
*  ### pm2 logs

Show all log files of applications managed by PM2.
```
root @ iot2040: ~ / s7-mqtt-opcuaserver-iot # pm2 logs
[TAILING] Tailing last 15 lines for [all] processes (change the value with --lines option)
/home/root/.pm2/pm2.log last 15 lines:
PM2 | 2019-05-07T12: 49: 07: PM2 log: App [s7-mqtt-opcuaserver: 0] starting in -fork mode-
PM2 | 2019-05-07T12: 49: 07: PM2 log: App [s7-mqtt-opcuaserver: 0] online
PM2 | 2019-05-08T02: 48: 27: PM2 log: Stopping app: s7-mqtt-opcuaserver id: 0
PM2 | 2019-05-08T02: 48: 28: PM2 log: pid = 8768 msg = failed to kill - retrying in 100ms
PM2 | 2019-05-08T02: 48: 28: PM2 log: pid = 8768 msg = failed to kill - retrying in 100ms
PM2 | 2019-05-08T02: 48: 28: PM2 log: App [s7-mqtt-opcuaserver: 0] exited with code [0] via signal [SIGINT]

/home/root/.pm2/logs/s7-mqtt-opcuaserver-error.log last 15 lines:
/home/root/.pm2/logs/s7-mqtt-opcuaserver-out.log last 15 lines:
0 | s7-mqtt-opcuaserver | S7 connection initalized ...
0 | s7-mqtt-opcuaserver | Start S7 Pub Polling ...
0 | s7-mqtt-opcuaserver | Start OPCUA Server ...
0 | s7-mqtt-opcuaserver | The primary Server endpoint URL is opc.tcp://192.168.200.1:4840/ua/node-opcua
0 | s7-mqtt-opcuaserver | OPCUA Server started.
0 | s7-mqtt-opcuaserver | OPCUA Server is now listening on port 4840 (press CTRL + C to stop Server).
0 | s7-mqtt-opcuaserver | UNSUPPORTED REQUEST !! FindServersOnNetworkRequest

```
Here you can check the address of the active OPC UA Server as ***URL endpoint***.

## Connecting to Server OPC UA 

Any application that have ***OPC UA Client*** function can be able to connect to OPC UA Server created via the *s7-mqtt-opcuaserver-iot* application.

Below is an example of connection with *UAExpert* application, 
downloadable here [UAExpert](https://www.unified-automation.com/products/development-tools/uaexpert.html). You must be registered on UAExpert website for downloading installer.

![ClientGIF](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/raw/master/docs/UAExpert_Client.gif)

The ***CA Certificate*** is produced by the script, ***./configure.sh***, which reads the IP configuration and the Host name from the device.
Check that the certificate is correctly recognized by the client and that the IP, DNS and URI parameters of the certificate are valid with respect to your system.

The OPC UA variables can be written only if the parameter ***"writable"*** has been configured in the configuration file ***app-config.json***
as described in point **App Configuration**.

## Installation on other systems

All *node_modules* are pre-compiled for IOT2040. so it is not necessary to run the npm install command to install the Node.JS modules.

To be able to use the application with other devices it must be re-installed all the necessary Node.JS modules, 
going to the app directory and using the *npm* command:

```
cd /opt/s7-mqtt-opcuaserver-iot
rm -rf node_modules
npm install
```
Wait for the installation to complete.

## Configuration Script

In order to be able to identify securely with its Clients, the *OPC UA* Server implemented uses a *CA Certificate* which must be specific to each device. 
The necessary files must be generated before the application starts.
The configuration script provided automatically creates the files with the names ***private_key.pem*** and ***certificate.pem***. 
These names must respect the values of the *"certificateFile"* and *"privateKeyFile"* properties of the ***app-config.json*** file

The application was developed using *Mosquitto MQTT Broker* (pre-installed in SIMATIC IOT2040) as an internal data exchange bus. 
In order to use other MQTT Brokers, configuration with the same characteristics as the files above is required.
(***mosquitto.conf, aclfile, auth***).

## Install Git on SIMATIC IOT2040

The ***"git"*** command is not installed by default on IOT2040 so you can install *git* by downloading and compiling the *git* sources directly.
Below the commands to be performed for the installation

```
curl -L https://github.com/git/git/archive/v2.17.0.tar.gz --output git-2.17.0.tar.gz
tar -zxf git-2.17.0.tar.gz
cd git-2.17.0
make prefix = /usr/local all
make prefix = /usr/local install
```
This installation may take a few hours but at the end the *git* command will work.

## References

* [node-snap7](https://github.com/mathiask88/node-snap7) - This is a node.js wrapper for snap7. Snap7 is an open source, 32/64 bit, multi-platform Ethernet communication suite for interfacing natively with Siemens S7 PLCs.
* [asyncawait](https://github.com/yortus/asyncawait) - asyncawait addresses the problem of callback hell in Node.js because of Node.JS V 6.X 
* [node-opcua](https://github.com/node-opcua/node-opcua) - an implementation of a OPC UA stack fully written in javascript and nodejs
* [PM2](https://github.com/Unitech/pm2) - PM2 is a production process manager for Node.js applications with a built-in load balancer. It allows you to keep applications alive forever, to reload them without downtime and to facilitate common system admin tasks.
* [mqtt](https://github.com/mqttjs/MQTT.js) - MQTT.js is a client library for the [MQTT](http://mqtt.org/) protocol, written in JavaScript for node.js and the browser.


## Release History

-   1.0.0
    -   The first proper release

## License

Distributed under the MIT License. See `LICENSE` for more information.

## Contributing

1.  Fork it ([https://github.com/yourname/yourproject/fork](https://github.com/yourname/yourproject/fork))
2.  Create your feature branch (`git checkout -b feature/fooBar`)
3.  Commit your changes (`git commit -am 'Add some fooBar'`)
4.  Push to the branch (`git push origin feature/fooBar`)
5.  Create a new Pull Request

## Contact

SIMATICIPC –  [SIMATICIPC@GitLab](https://gitlab.com/SIMATICIPC/)
