#!/bin/bash
echo "######################################################################"
echo "####################### Generate CA Certificates #####################"
echo "############## Creating your new certificates. Wait! #################"

#####GENERATE SELF SIGNED CERTIFICATES FOR OPCUA SERVER
#READ HOSTNAME AND COPY CONFIG TEMPLATE IN A NEW CONFIG FILE WITH NEW HOSTNAME
HOSTNAME=$(hostname)
sed "s/#HOST/${HOSTNAME}/g" openssl.cnf > myopenssl.cnf
echo "your hostname is ${HOSTNAME}"

#####
#FOR EACH INTERFACES READ IP AND REPLACE IN  CONFIG FILE
INTERFACES=$(ip link | awk -F: '$0 !~ "lo|vir|wl|^[^0-9]"{print $2;getline}')
i=1
for n in $INTERFACES
do
	IPS=$(ifconfig | grep -A 1 ${n} | tail -1 | cut -d ':' -f 2 | cut -d ' ' -f 1)

	#CHECK IF IS A VALID IP
	if [[ $IPS =~ "." ]];
	then
		sed  -i "s/#IP${i}/IP.${i}   = ${IPS}/g" myopenssl.cnf
		echo "find ${n} with ip address ${IPS}"
	fi
    	
    	i=$((i+1))
done
echo "OpenSSL config generated."

#GENERATE KEY AND CERT
openssl req -x509 -days 365 -nodes -newkey rsa:2048 -keyout private_key.pem \
-out certificate.pem -config myopenssl.cnf


