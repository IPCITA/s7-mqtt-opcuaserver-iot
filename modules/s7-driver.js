/* global require */
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var snap7 = require('node-snap7');
var mqtt = require('mqtt');

const areaLenTypeDict = {
                        'Real' : 4,
                        'Byte' : 1,
                        'Word' : 2,
                        'DWord': 4,
                        'Int': 2,
                        'UInt': 2,
                        'DInt': 4,
                        'UDInt': 4,
						'Bool' : 1
};
const areaTypeDict = {
						'PE' : snap7.S7Client().S7AreaPE,
						'PA' : snap7.S7Client().S7AreaPA,
						'MK' : snap7.S7Client().S7AreaMK,
						'DB' : snap7.S7Client().S7AreaDB,
						'CT' : snap7.S7Client().S7AreaCT,
						'TM' : snap7.S7Client().S7AreaTM
};


function S7EncodeParam(value, S7Config) {
	var S7Buffer;
	try
	{
		
		switch(S7Config.dataType)
		{
			case 'Real':
			{
				S7Buffer = Buffer.allocUnsafe(4);
				S7Buffer.writeFloatBE(value, 0);
			}
			break;

			case 'Byte':
			{
				S7Buffer = Buffer.allocUnsafe(1);				
				S7Buffer.writeUInt8(value, 0);			
			}
			break;

			case 'Word':
			{
				S7Buffer = Buffer.allocUnsafe(2);				
				S7Buffer.writeUInt16BE(value, 0);			
			}
			break;

			case 'DWord':
			{
				S7Buffer = Buffer.allocUnsafe(4);				
				S7Buffer.writeUInt32BE(value, 0);		
			}
            break;

            case 'Int':
            {
				S7Buffer = Buffer.allocUnsafe(2);				
				S7Buffer.writeInt16BE(value, 0);
            }
            break;

            case 'UInt':
            {
                S7Buffer = Buffer.allocUnsafe(2);				
			    S7Buffer.writeUInt16BE(value, 0);
            }
            break;

            case 'DInt':
            {
                S7Buffer = Buffer.allocUnsafe(4);			
			    S7Buffer.writeInt32BE(value, 0);
            }
            break;

            case 'UDInt':
            {
                S7Buffer = Buffer.allocUnsafe(4);			
			    S7Buffer.writeUInt32BE(value, 0);
            }
            break;

            case 'Bool':
            {
            	let nVal = 0;                
                if (value)
                    nVal = 1;
                else
                    nVal = 0;
               S7Buffer = Buffer.allocUnsafe(1);
			   S7Buffer.writeUInt8(nVal, 0);
            }
            break;

            case 'String':
            {
                let strLen = value.length;
                S7Buffer = Buffer.allocUnsafe(S7Config.Amount);
                S7Buffer.writeUInt8((S7Config.Amount - 2), 0);
                S7Buffer.writeUInt8(strLen, 1);
                S7Buffer.write(value, 2);
            }           
            break;
            
			default:
				throw 'Invalid datatype';
				break;		
		}		
	}
	catch (e)
	{
		return e
	}
	return S7Buffer
}
function S7DeviceWriteParam(S7driver, value, varConfig)
{
	return new Promise(async (function(res, err)
	{
		let S7Buffer;
		try
		{
			S7Buffer = S7EncodeParam(value, varConfig);
		}
		catch (e)
		{
			return err('S7 Enconding error ' + varConfig.name);
		}

		S7driver.WriteArea (varConfig.Area, varConfig.DBNumber, varConfig.Start, varConfig.Amount, varConfig.WordLen, S7Buffer, function (error)
			{
				if (error)
					return err('Error during writing parameter ' + varConfig.name);
				else
					return res();
			});
	}));
}
var S7writeClient = async (function(S7Client, S7Var, value)
{
	try 
	{
        //write to S7
        await (S7DeviceWriteParam(S7Client.driver, value, S7Var));
        //console.log("Write Successful",);
	}
	catch (e)
	{
		console.log(e);
	}
});
function subClientWrite (S7Client, mqttClients)
{
	//for each client
	for (let i = 0; i < mqttClients.length; i++)
	{
		let client = mqttClients[i];
		//
		if (client.writable)
		{
			//search in dataset for S7var
			for (let j = 0; j < S7Client.dataSet.length; j++)
			{
				let varName = client.name.slice(client.name.search("/") + 1);
				if (varName === S7Client.dataSet[j].name)
				{
					//store var param
					let S7Var = S7Client.dataSet[j];
					//sub and listen
					client.mqtt.on ('message', function (topic, message)
					{
						//decode message
						let msg = JSON.parse (message.toString('utf-8'));
						//write to S7
						S7writeClient (S7Client, S7Var, msg.value)
					});
				}
			} 
		}
	}
}
function createSingleClient(deviceName, datapoint)
{
    return new Promise (function (res, err)
    {
        let client = {};
        client.name = deviceName + "/" + datapoint.name;
        client.dataType = datapoint.dataType;
        client.writable = datapoint.writable;
    
        let config =
        {
            port : 41883,
            host : 'localhost',
            keepalive : 10000,
            clientId : 'snap7-' + client.name,
            username : 'node',
            password : 'node'
        };
    
        client.mqtt = mqtt.connect (config);
    
        // client.mqtt.on('connect', function () 
        // {
        //     console.log (config.clientId, "connected.");
        // });

        client.mqtt.subscribe ('/server/'+ client.name, function (err) 
        {
            if (!err) 
            {
                //client.mqtt.publish ('/server/'+ client.name, 'Hi from '+ config.clientId);
                return res (client);
            }
            else return err();
        });
    });
}
function createClients (S7Device)
{
    return new Promise (function (res, err)
    {
        var clients = [];
        for (let i = 0; i < S7Device.variablesList.length; i++)
        {
	    //create each client with a promise for each variable fulfilled when client subscribe
            let datapoint = S7Device.variablesList[i];
            let client = createSingleClient (S7Device.deviceName, datapoint);
            clients.push (client);
        }

        Promise.all(clients)
                .then((results) => {

                    return res(results)
                })
                .catch((e) => {
                    console.log (e);
                    return err(e);
                });
    });
}
function setElement (driver, oVar) {
		//create the object to be read
		let request = {};
		request.name = oVar.name;
		request.dataType = oVar.dataType;
		request.Area = areaTypeDict[oVar.Area];
		
		if (oVar.Area == "DB")
		{
			request.DBNumber = oVar.DBNumber;
		}
		else
		{
			request.DBNumber = 0;
		}
		
		if (oVar.dataType == "String")
		{
			request.WordLen = driver.S7WLByte;
			request.Start = oVar.offset;
			request.Amount = oVar.stringLen + 2;
		}
		else if (oVar.dataType == "Bool"){
			request.WordLen = driver.S7WLBit;
			request.Start = (oVar.offset * 8) + oVar.index;
			request.Amount = areaLenTypeDict[oVar.dataType];
		}
		else 
		{
			request.WordLen = driver.S7WLByte;
			request.Start = oVar.offset;
			request.Amount = areaLenTypeDict[oVar.dataType];
		}			
		
		return request
}
function SetS7Request (S7Client, varList)
{
	let requestSet = [];
	// prepare an array of Readable s7 variables
	for (let i = 0; i < varList.length; i++) 
	{
		// initialize variable for S7Read function and var configuration from config file
		let request = {};
		let varConfig = varList[i];
		
		//initialize a new object for setElement() function
		let oVar ={};
		oVar.name = varConfig.name;
		oVar.Area = varConfig.Area;
		oVar.DBNumber = varConfig.DBNumber;
		oVar.dataType = varConfig.dataType;
		oVar.offset = varConfig.offset;
		
		//special parameters for String and Bool
		if (oVar.dataType == "String")
		{
			oVar.stringLen = varConfig.stringLen;
		}
		else if (oVar.dataType == "Bool")
		{
			oVar.index = varConfig.index;
		}
		
		//create the object to be read
		request = setElement(S7Client.driver, oVar);
		requestSet.push(request);
	}
	//return request array of s7 variables
	return requestSet 
}
module.exports.S7Initialize = async ( function (S7Config)
{
	var S7Clients = [];
	//for each S7 Device in array
	for (let i = 0; i < S7Config.S7Devices.length; i++)
	{
		//store actual s7 device
		let device = S7Config.S7Devices[i];
		try
		{		
			let S7Client = {};
			S7Client.deviceName = device.deviceName;
			S7Client.driver = new snap7.S7Client();
			S7Client.connected = false;
			S7Client.forceDisconnection = false;
			S7Client.dataSet = SetS7Request(S7Client, device.variablesList);
			S7Client.mqttClients = await (createClients (device));
			subClientWrite(S7Client, S7Client.mqttClients);
			S7Clients.push (S7Client);
		}
		catch (e)
		{
			console.log(e);
			return e
		}
	}
	return S7Clients
});

function clientPublish (deviceName, mqttClients, publishSet)
{
	//for each variable read
    for (let i = 0; i < publishSet.length; i++)
    {
		//store value
		let value = publishSet[i].value;
		//search for mqtt client
		for (let j = 0; j < mqttClients.length; j++)
		{
			let varName = mqttClients[j].name.slice(mqttClients[j].name.search("/") + 1);
			if (publishSet[i].name === varName)
			{
				//create msg
				let msg = {'dataType' : mqttClients[j].dataType, 'value' : value};
        		try
        		{
					let topic = '/server/' + mqttClients[j].name;
					//pub msg to /server/<deviceName>/<var-name>
            		mqttClients[j].mqtt.publish (topic, JSON.stringify(msg));
        		}
        		catch (e)
        		{
            		console.log (e);
				}
			}	           
		}
	}
}
function S7DecodeValue(S7Buffer, S7Variable)
{
	try
	{
		var param = [];
		switch(S7Variable.dataType) {
			case 'Real': {
				param.push(S7Buffer.readFloatBE(0));
			}
			break;

			case 'Byte': {
				param.push(S7Buffer.readUInt8(0));		
            }
            break;

            case 'Word': {
				param.push(S7Buffer.readUInt16BE(0));
            }
            break;

			case 'DWord': {
				param.push(S7Buffer.readUInt32BE(0));
            }
            break;

            case 'Int': {
                param.push(S7Buffer.readInt16BE(0));
            }
            break;

            case 'UInt': {
                param.push(S7Buffer.readUInt16BE(0));
            }
            break;

            case 'DInt': {
                param.push(S7Buffer.readInt32BE(0));
            }
            break;

            case 'UDInt': {
                param.push(S7Buffer.readUInt32BE(0));
            }
            break;

            case 'Bool': {
                let bVal = S7Buffer.readUInt8(0);
                if (bVal)
                    param.push(true);
                else
                    param.push(false);
            }
            break;

            case 'String': {
            	// Get the actual length of encoded string (Byte 1 of a String variable)
            	let strActLen = S7Buffer.readUInt8(1);
            	let strVal = S7Buffer.toString('utf8', 2, 2 + strActLen);
            	param.push(strVal);
            }
            break;

			default:
				throw 'Invalid datatype';
				break;		
		}

		return param;
	}
	catch (e)
	{
		throw e;
	}	
}
function S7ReadSingle(S7driver, requestVar)
{
	return new Promise(function(res, err)
	{
			S7driver.ReadArea(requestVar.Area, requestVar.DBNumber, requestVar.Start, requestVar.Amount, requestVar.WordLen, function(error, result)
			{
				if (error)
				{				
					return err(error);
				}
				else
				{
					// Return the buffer filled with read data
					return res(result);
				}
			});
		});
}
var S7Read = async (function (S7Client, requestSet)
{
	var result = [];

	// Read each value from device 
	for (let i = 0; i < requestSet.length; i++)
	{
		let res = {};
		try
		{
			res.data = await (S7ReadSingle(S7Client.driver, requestSet[i]));
			res.error = 0;
			result.push(res);
		}
		catch (e)
		{
			res.error = S7Client.driver.ErrorText(e);
			res.data = null;
			result.push(res);
			console.log('Error reading variable', requestSet[i].name, "with error:", res.error);
		}
	}
	
	var varList = [];
	
	// Decode each value
	for (let i = 0; i < requestSet.length; i++) 
	{
        if (!(result[i].error === undefined) && result[i].error == 0)
		{            
			let S7var = {};
			let varValue = S7DecodeValue(result[i].data, requestSet[i]);

			S7var.name = requestSet[i].name;
			S7var.value = varValue[0];
			S7var.dataType = requestSet[i].dataType;
			
			varList.push(S7var);
		}
		else
		{
            console.log('Error reading variable ' + requestSet[i].name);
            S7Client.forceDisconnection = true;
		}
	}
	
	return varList;	
});
function S7Disconnect(S7driver)
{
	let ret = S7driver.Disconnect();		
}
function SetS7Connection(S7Client, deviceConfig) {
    
    return new Promise( function(res, err)
    {		
		S7Client.driver.ConnectTo (deviceConfig.ipAddress, deviceConfig.rack, deviceConfig.slot, function(errorcode) {
			if (errorcode)
			{
				S7Client.connected = false;
                // Reject
                let  error = S7Client.driver.ErrorText (errorcode);
				return err('ERROR : '+ error + ' device: ' + deviceConfig.ipAddress + ',' + deviceConfig.rack + ',' + deviceConfig.slot + ')');
			}
			else
			{
				S7Client.connected = true;			
				// Resolve
				return res(S7Client);
			}
		});
	});	
}
module.exports.S7ReadClients = async(function(S7Client, deviceConfig, dataSet)
{	
	try
	{
        // Disconnect from device, if requested
		if (S7Client.forceDisconnection)
		{
			S7Disconnect(S7Client.driver);
			S7Client.forceDisconnection = false;
			S7Client.connected = false;
		}
		// If the client is not connected, try to connect
		if (!(S7Client.driver.Connected()) && !(S7Client.connected))
		{
            for (let retry = 0; retry < 5; retry ++)
            {
                try 
			    {
                    await (SetS7Connection (S7Client, deviceConfig));
                    break
				}
			    catch (e) 
			    {
				    console.log(e);
                }
            }
		}
		//if the client is connected then read data
		if (S7Client.driver.Connected() && S7Client.connected) {
			try 
			{
				// Read device parameters
				var responseSet = await (S7Read (S7Client, dataSet));
				// publish to mqtt
				clientPublish (S7Client.deviceName, S7Client.mqttClients, responseSet);
				return responseSet; 
			}
			catch (e) 
			{
				throw e;
			}
		}
	}
	catch (e)
	{		
		console.log (e);
		return e;
	}
});
