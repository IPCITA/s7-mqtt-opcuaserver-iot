/* global require */
var async = require('asyncawait/async');
var await = require('asyncawait/await');
const opcua = require('node-opcua');
var ip = require("ip");
var os = require ("os");
var mqtt = require('mqtt');

//local cache object for mqtt subscribers
var vCache = {};

//datatype mapping between S7 and OPC UA standards
const moduleTypesMap = 
{
    'Bool' : 'Boolean',
    'Real' : 'Float',
    'Double' : 'Double',
    'Byte' : 'Byte',
    'Int' : 'Int16',
    'DInt' : 'Int32',
    'LInt' : 'Int64',
    'UInt' : 'UInt16',
    'UDInt' : 'UInt32',
    'ULInt' : 'UInt64',    
    'String' : 'String',
    'Word' : 'UInt16',
    'DWord' : 'UInt32',
};
const moduleTypesCodeMap = 
{
    'Boolean' : opcua.DataType.Boolean,
    'Float' : opcua.DataType.Float,
    'Double' : opcua.DataType.Double,
    'Byte' : opcua.DataType.Byte,
    'Int16' : opcua.DataType.Int16,
    'Int32' : opcua.DataType.Int32,
    'Int64' : opcua.DataType.Int64,
    'UInt16' : opcua.DataType.UInt16,
    'UInt32' : opcua.DataType.UInt32,
    'UInt64' : opcua.DataType.UInt64,
    'String' : opcua.DataType.String
};

function opcuaAddDevice(deviceName, mqttClients, uaNodeList, addressSpace, namespace)
{
    // Add device object node to the namespace    
    let objNode = namespace.addObject({
        organizedBy : addressSpace.rootFolder.objects,
        browseName : deviceName
    });

    // Add each tag (variable) to server
    for (let i = 0; i < mqttClients.length; i++) 
    {
        let varName = mqttClients[i].name.slice(mqttClients[i].name.search("/") + 1);
        let dpNodeId = 's="' + deviceName + '"."' + varName + '"';

        if (!mqttClients[i].writable)
        {
            namespace.addVariable({
            componentOf : objNode,
            browseName : varName,
            dataType : moduleTypesMap[mqttClients[i].dataType],
            nodeId : dpNodeId,
            value :
            {
                get : function() 
                {
                    let myValue = vCacheGet(mqttClients[i].name, mqttClients[i].dataType);
                    let myDataType = moduleTypesCodeMap[moduleTypesMap[mqttClients[i].dataType]];
                    //write value to server
                    return new opcua.Variant({ dataType: myDataType, value: myValue });
                },
                set: function (variant)
                {
                    return opcua.StatusCodes.BadNotWritable;
                }
            }
            });
        }
        else
        {
            namespace.addVariable({
            componentOf : objNode,
            browseName : varName,
            dataType : moduleTypesMap[mqttClients[i].dataType],
            nodeId : dpNodeId,
            value :
            {
                get : function() 
                {
                    let myValue = vCacheGet(mqttClients[i].name, mqttClients[i].dataType);
                    let myDataType = moduleTypesCodeMap[moduleTypesMap[mqttClients[i].dataType]];
                    //write value to server
                    return new opcua.Variant({ dataType: myDataType, value: myValue });
                },
                set : function(variant)
                {
                    //create message
                    let msg = {'dataType' : mqttClients[i].dataType, 'value' : variant.value};
                    let topic =  '/server/' + mqttClients[i].name;
                    //pub msg to /server/<deviceName>/<var-name>
                    mqttClients[i].mqtt.publish (topic, JSON.stringify(msg));
                    return opcua.StatusCodes.Good;
                }
            }
            });
        }        
    }     
    uaNodeList.push(objNode);
}
function opcuaInitializeAsync(server) 
{
    return new Promise(function(res, err)
    {
        server.initialize(function ()
        {
            return res();
        });			
	});
}
function opcuaStartAsync (server)
{
    return new Promise(function (res, err)
    {
            server.start(function () 
            {
                const endpointUrl = server.endpoints[0].endpointDescriptions()[0].endpointUrl;
                console.log("The primary Server endpoint URL is", endpointUrl);
                return res();
            });
    });
}
function subClientWrite (mqttClients)
{
    //for each client
    for (let i = 0; i < mqttClients.length; i++)
    {
        let client = mqttClients[i];
        //create a listener for write to cache
        client.mqtt.on ('message', function (topic, message)
		{
            //decode message
			let msg = JSON.parse (message.toString('utf-8'));
			//write to vCache
			vCachePut (client.name, msg.value);
        });
    }
}
function createSingleClient (deviceName, datapoint)
{
    return new Promise (function (res, err)
    {
        let client = {};
        client.name = deviceName + "/" + datapoint.name;
        client.dataType = datapoint.dataType;
        client.writable = datapoint.writable;
    
        let config =
        {
            port : 41883,
            host : 'localhost',
            keepalive : 10000,
            clientId : 'opcua-' + client.name,
            username : 'node',
            password : 'node'
        };
    
        client.mqtt = mqtt.connect (config);
    
        // client.mqtt.on('connect', function () 
        // {
        //     console.log (config.clientId, "connected.");
        // });

        client.mqtt.subscribe ('/server/'+ client.name, function (err) 
        {
            if (!err) 
            {
                //client.mqtt.publish ('/server/'+ client.name, 'Hi from '+ config.clientId);
                return res (client);
            }
            else return err();
        });
    });
}
function createClients (S7Device)
{
    return new Promise (function (res, err)
    {
        var clients = [];
        for (let i = 0; i < S7Device.variablesList.length; i++)
        {
            let datapoint = S7Device.variablesList[i];
            let deviceName = S7Device.deviceName;
            
            let client = createSingleClient (deviceName, datapoint);
            clients.push (client);
        }

        Promise.all(clients)
                .then((results) => {
                    subClientWrite(results);
                    return res(results)
                })
                .catch((e) => {
                    console.log (e);
                    return err(e);
                });
    });
}
var mqttStart = async (function (appConfig)
{
    var mqttModules = [];
    for (let i = 0; i < appConfig.S7Devices.length; i++)
    {
        let device = appConfig.S7Devices[i];
        let mqttClients = await (createClients (device));
        mqttModules.push (mqttClients);
    }
    return mqttModules
});
module.exports.startAsync = async (function (appConfig)
{
    let moduleData = {};
    //Read hostname and IPAddress
    var hostname = os.hostname();
    var ipAddress = ip.address();
    
    //Set some Options
    let options = {};
    options.port = appConfig.serverConfig.port;
    options.resourcePath = "ua/node-opcua";
    options.alternateHostname = ipAddress;
    options.buildInfo = 
    {
        productName: hostname,
        buildNumber: "1",
        buildDate: new Date(2019,1,1)
    };    
    options.serverInfo = 
    {
        applicationUri : "urn:"+ hostname + ":ua/node-opcua",
        productUri : "node-opcua",
        applicationName : {text: "node-opcua", locale:"en"},
        gatewayServerUri : null,
        discoveryProfileUri : null,
        discoveryUrls : []
    };

    // Use custom certificate and private key if .pem files are present
    if ((appConfig.serverConfig.certificateFile.includes (".pem")) && (appConfig.serverConfig.privateKeyFile.includes (".pem")))
    {
        options.certificateFile = appConfig.serverConfig.certificateFile;
        options.privateKeyFile = appConfig.serverConfig.privateKeyFile;
    }
    //set user management
    options.allowAnonymous = appConfig.serverConfig.allowAnonymous;
    // Use accounts for identity verification if Anonymous is not allowed
    if (!options.allowAnonymous)
    {
        let usrManager = {};
        usrManager.isValidUser = function(userName, password)
        {
            //for each user in users list
            for (let i = 0; i < appConfig.serverConfig.userList.length; i++) 
            {
                let uName = appConfig.serverConfig.userList[i].user;
                let uPass = appConfig.serverConfig.userList[i].password;
                
                if ((uName === userName) && (uPass === password))
                {
                    //authenticate    
                    return true;
                }
            }
            return false;
        };
        options.userManager = usrManager;
    }

    //array of mqtt clients
    moduleData.mqttModules = await (mqttStart (appConfig));
    

    // Build server object
    moduleData.server = new opcua.OPCUAServer(options);

    // Initalize the server
    try
    {
        await (opcuaInitializeAsync(moduleData.server));
    }
    catch (e)
    {
        console.log("OPCUA Server initialization failed : " + e);
    }

    // Build address space
    const addressSpace = moduleData.server.engine.addressSpace;
    const namespace = addressSpace.getOwnNamespace();    
    moduleData.uaNodeList = [];

    //for each S7 device add one folder and its variables
    for (let i = 0; i < appConfig.S7Devices.length; i++)
    {
        let deviceName = appConfig.S7Devices[i].deviceName;
        opcuaAddDevice(deviceName, moduleData.mqttModules[i], moduleData.uaNodeList, addressSpace, namespace);
    }

    // Start the server
    try
    {
        await (opcuaStartAsync(moduleData.server));
    }
    catch (e)
    {
        console.log("OPCUA Server start failed : " + e);
    }

    console.log("OPCUA Server started.");
    //store some values from server and make the magic..
    moduleData.connected = true;
    let port = moduleData.server.endpoints[0].port;
    moduleData.endpointUrl = moduleData.server.endpoints[0].endpointDescriptions()[0].endpointUrl;
    
    console.log("OPCUA Server is now listening on port", port,"(press CTRL+C to stop Server).");

    return moduleData;
});

function vCacheGet(name, dataType)
{
    if (vCache[name] == undefined)
    {
        let retObj = 0;
        if (dataType == 'Bool')
        {
            retObj = false;
        }
        else if (dataType == 'String')
        {
            retObj = '';            
        }
        vCache[name] = retObj;
        return retObj;
    }       
    else 
    {
        return vCache[name];
    }                 
}
function vCachePut(name, value)
{
        // Update with last value
        vCache[name] = value;       
}
