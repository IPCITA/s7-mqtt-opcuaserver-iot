# s7-mqtt-opcuaserver-iot
![enter image description here](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/raw/master/docs/app-workflow.png)
### A simple OPC UA Server that interacts with SIEMENS S7 Devices

Applicazione basata su Node.JS v6.x con cui è possibile interfacciare diversi dispositivi SIEMENS S7 ad un Server basato su protocollo OPC UA, mettendo così a disposizione i diversi dati dei controllori S7 per la lettura / scrittura tramite Client OPC UA connessi.

 - Connessione multipla di diversi SIMATIC PLC e altri dispositivi SIEMENS S7;
 - Configurabile semplicemente tramite file di configurazione;
 - OPC UA Server con opzione *"Sign&Encrypt"* e autenticazione utenti integrata;
 - Comunicazione bi-direzionale S7 - OPCUA Server per scrittura/lettura variabili.

## Table of Contents

-   [Before Starting](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/docs/README_ITA.md#before-starting)
-   [Getting Started](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/docs/README_ITA.md#getting-started)
-   [First Configuration](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/docs/README_ITA.md#first-configuration)
-   [App Configuration](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/docs/README_ITA.md#app-configuration)
    -   [OPC UA Server Configuration](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/docs/README_ITA.md#opcu-ua-server.configuration)
    -   [S7 Devices Configuration](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/docs/README_ITA.md#s7-devices-configuration)
 -   [Run the application](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/docs/README_ITA.md#run-the-application)
 -   [Manage Startup and Restart with PM2 App](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/docs/README_ITA.md#manage-startup-and-restart-with-pm2-app)
 -   [Connecting to Server OPC UA ](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/docs/README_ITA.md#connecting-to-server-OPC-UA )
 -   [Installation on other systems](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/docs/README_ITA.md#installation-on-other-systems) 
-   [Configuration Script](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/docs/README_ITA.md#configuration-script)
-   [Install Git on SIMATIC IOT2040](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/docs/README_ITA.md#install-GIT-on-SIMATIC-IOT2040)
-   [References](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/docs/README_ITA.md#references)
-   [Release History](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/docs/README_ITA.md#release-history)
-   [Contributing](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/docs/README_ITA.md#contributing)
-   [License](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/docs/README_ITA.md#license)
-   [Contact](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/blob/master/docs/README_ITA.md#contact)

## Supportata solo Example_Image V2.2!

## Before Starting

Tramite questa guida è possibile installare con successo l'app  *s7-mqtt-opcuaserver-iot*. Per iniziare consulta il capitolo **Getting Started**.

L'applicazione viene fornita pre-compilata per SIMATIC IOT2040. Per l'utilizzo con altre piattaforme consulta il capitolo **Installation on other systems**.

L'applicazione utilizza *Mosquitto MQTT Broker* (pre-installato in SIMATIC IOT2040) come bus di scambio dati interno. Per poter utilizzare correttamente l'applicazione è necessario configurare l'applicazione Mosquitto. Viene fornito lo script _**./configure.sh**_ con cui vengono automaticamente effettuati alcuni passaggi necessari alla configurazione. Per maggiori informazioni consulta il capitolo **First Configuration**.

Prima di utilizzare l'applicazione è necessario configurare le opzioni del *Server OPC UA* e i parametri dei dispositivi *SIEMENS S7* a cui è necessario connettersi.  La configurazione è possibile tramite il file ***app-config.json*** dell'app. Per maggiori informazioni consulta il capitolo **App Configuration**.

Tramite il tool **PM2** può essere gestita automaticamente l'esecuzione di questa applicazione. Con PM2 ad ogni nuovo boot del sistema l'applicazione *s7-mqtt-opcuaserver-iot* viene avviata e in caso di errori viene automaticamente riavviata per consentire continuità al funzionamento.
Viene fornito lo script ***./install-pm2.sh*** con cui vengono automaticamente effettuati i passaggi necessari all'installazione e alla configurazione di PM2. Per maggiori informazioni consulta il capitolo **Manage Startup and Restart with PM2 App**.

## Getting Started

Aprire un terminale del sistema e recarsi nella cartella */opt* (o nella cartella desiderata). Clonare la directory dell'app *s7-mqtt-opcuaserver-iot* dalla repository GitLab (Per installare il comando *git* su IOT2040 consultare il paragrafo  **Install Git on SIMATIC IOT2040**).
oppure scaricarla come archivo compresso, decomprimerla e copiarla nella cartella */opt* (o nella cartella desiderata).

```
cd /opt
git clone https://gitlab.com/IPCITA/s7-mqtt-opcuaserver-iot.git
```

Tutti i *node_modules* sono pre-compliati per IOT2040. per cui non è necessario effettuare il comando npm install per installare i moduli Node.JS. Per l'utilizzo con altre piattaforme consulta il capitolo **Installation on other systems**.


## First Configuration

Viene fornito lo script ***./configure.sh*** con cui vengono automaticamente effettuati alcuni dei passaggi necessari alla configurazione:

 - copia dei file di configurazione corretti nella cartella di installazione di Mosquitto
 - generazione files per Certificato CA di sicurezza con hostname e Indirizzo IP automatico

Per poter avviare lo script recarsi sul dispositivo nella cartella dell'app e avviare lo script
```
cd /opt/s7-mqtt-opcuaserver-iot
chmod +x ./configure.sh
./configure.sh
```
Durante l'esecuzione dello script verranno mostrate informazioni sui vari passaggi effettuati e sulle successive azioni da compiere. Di seguito un esempio di ***output*** dell'esecuzione di *./configure.sh*
```
root@iot2040:~ cd /opt/s7-mqtt-opcuaserver-iot
root@iot2040:~/s7-mqtt-opcuaserver-iot# chmod +x ./configure.sh
root@iot2040:~/s7-mqtt-opcuaserver-iot# ./configure.sh
######################################################################
############# Copying Mosquitto MQTT configuration files #############
cp ./mqtt/mosquitto.conf /etc/mosquitto
cp ./mqtt/aclfile /etc/mosquitto
cp ./mqtt/auth /etc/mosquitto
######################################################################
####################### Generate CA Certificates #####################
############## Creating your new certificates. Wait! #################
your hostname is iot2040
Find eth0 with IP Address 192.168.200.1
Find eth1 with IP Address 192.168.1.180
OpenSSL config generated.
Generating a 2048 bit RSA private key
............+++
....+++
writing new private key to './cert/private_key.pem'
-----
######################################################################
###################### CONFIGURATION COMPLETED! ######################
######################################################################
### SET AUTOSTART OF MOSQUITTO SERVICE USING iot2000setup COMMAND. ###
############ GO TO Software -> Manage Autostart Options AND ##########
############ ENABLE Autostart Mosquitto Broker.'            ##########
######################################################################
##### AFTER SETTINGS ABOVE REBOOT YOUR SYSTEM TO APPLY SETTINGS! #####
############### RUN install.sh AFTER REBOOT FOR SET       ############
############### AUTOSTART s7-mqtt-opcuaserver-iot AT BOOT.############
######################################################################
```
Per maggiori dettagli sulle operazioni eseguite dallo script *./configure.sh* consulta il capitolo **Configuration Script**.

Prima di procedere all''avvio dell'applicazione è necessario abilitare l'***avvio automatico*** al boot del servizio *Mosquitto MQTT Broker*. Tramite il comando integrato ***iot2000setup*** presente su SIMATIC IOT2040 questa operazione può essere effettuata facilmente:
```
iot2000setup
```
![enter image description here](https://hackster.imgix.net/uploads/attachments/405371/Screenshot_20171222_143951.png?auto=compress,format&w=500&h=350)

Nel menù ***Software*** -> *Manage Autostart Options* è necessario solamente abilitare la voce *Autostart Mosquitto Broker*.
Al termine delle operazioni descritte effetuare un riavvio del dispositivo:
```
reboot
```

## App Configuration

Per modificare i parametri del Server OPC UA agire sul file ***./config/app-config.json***.  Questo file contiene tutti i parametri necessari alla configurazione dell'applicazione.
Nella prima parte del file si trovano due parametri a scopo informativo sull'applicazione:
```
{
	"appName" : "s7-mqtt-opcuaserver-iot",
	"description" : "Sample configuration file for two PLC S7-1500",
	......................
```
Dopodichè la configurazione viene divisa in due parti principali, ***serverConfig*** e ***S7Devices***.

### OPC UA Server Configuration
Nella proprietà ***"serverConfig"*** è possibile modificare la porta su cui il Server OPC UA potrà essere 
raggiunto (parametro ***"port"*** ), il tempo minimo di campionamento [ms] delle variabili lette dai dispositivi 
SIEMENS S7 con il parametro ***"cycleTime"*** ed opzionalmente, tramite il parametro ***"allowAnonymous"*** , 
abilitare o disabilitare l'autenticazione al Server OPC UA basata sulla lista utenti definita con la proprietà 
array ***"userList"*** personalizzabile con le proprie utenze. 

> Se ***"allowAnonymous"*** viene impostata su *false* non sarà possibile accedere al Server senza utente e password configurati inseriti da parte del Client OPC UA. 
 
> Con SIMATIC IOT2040 il parametro ***"cycleTime"*** non dovrebbe mai essere impostato al di sotto di 500 ms per un numero di tags o connessioni elevato.

```
	................................ 
	"serverConfig" : 
	{
			"cycleTime" : 500,
			"port" : 4840,
			"certificateFile": "./cert/certificate.pem",
			"privateKeyFile": "./cert/private_key.pem",
			"allowAnonymous" : false,
			"userList" : 
			[
				{
					"user" : "Siemens" ,
					"password" : "Siemens"
				},
				{
					"user" : "user" ,
					"password" : "user"
				}
			]
	},
	.............................
```
### S7 Devices Configuration
Nella seconda parte della configurazione troviamo l'array ***S7Devices***, che contiene la configurazione dei diversi controllori SIEMENS S7 e delle loro variabili.
Ogni elemento dell'array è un oggetto che rappresenta tutte le informazioni associate ad un singolo dispositivo SIEMENS S7. 

Il parametro  **"deviceName"** indica il nome del dispositivo S7 e sarà il corrispettivo nome dell'oggetto "Cartella" del Server OPC UA. E' possibile inserire anche una descrizione del device per scopo informativo. 

> ***Nota :*** Il parametro ***"deviceName"*** NON deve contenere il carattere *"/"* in quanto tipico del protocollo MQTT. L'utilizzo può portare a malfunzionamenti del Data Bus MQTT.

Di seguito un **esempio** di configurazione di *S7Devices* con l'utilizzo di due PLC *SIEMENS S7-1500*. Successivamente sono indicati tutti i dettagli sul significato dei vari parametri.
```
    ...............................
    "S7Devices" : 
	[
		{
			"deviceName" : "PLC_1",
			"description" : "S7-1500 PLC_1 TEST",
			"deviceConfig" : 
			{
					"ipAddress" : "192.168.200.10",
					"rack" : 0,
					"slot" : 1
			},
			"variablesList": 
			[
				{
						"name": "Tag0",
						"Area" : "DB",
						"DBNumber": 1,
						"offset": 6,
						"dataType": "Int",
						"writable": false
				},
				{
						"name": "Tag1",
						"Area" : "DB",
						"DBNumber": 1,
						"offset": 8,
						"dataType": "Int",
						"writable": false
				},
				{
						"name": "Tag2",
						"Area" : "DB",
						"DBNumber": 1,
						"offset": 10,
						"dataType": "Int",
						"writable": false
				}
				..........and other tags........
			]
		},
		{
			"deviceName" : "PLC_2",
			"description" : "S7-1500 PLC_2 TEST",
			"deviceConfig" : 
			{
					"ipAddress" : "192.168.200.10",
					"rack" : 0,
					"slot" : 1
			},
			"variablesList": 
			[
				{
						"name": "Tag13",
						"Area" : "DB",
						"DBNumber": 1,
						"offset": 36,
						"stringLen" : 254,
						"dataType": "String",
						"writable": true
				},
				{
						"name": "Tag14",
						"Area" : "DB",
						"DBNumber": 1,
						"offset": 292,
						"index" : 0,
						"dataType": "Bool",
						"writable": true
				}
				..........and other tags........
			]
		}
		..........and other S7 Devices........
	]	 
}            
```
In **"deviceConfig"** vengono indicati i parametri di connessione di ogni dispositivo SIEMENS S7. 
Inserire qui i valori corretti di **"ipAddress"**, ***"rack"*** e ***"slot"*** del singolo dispositivo.

| S7 Model | Rack | Slot |
| ------ | ------ | ------ |
| S7-1500, S7-1200, ...| 0 | 1 |
| S7-300, LOGO!, ... | 0 | 2 |

La proprietà **"variablesList"** è un array che elenca tutte le variabili desiderate da collegare al Server OPC UA e le loro proprietà.

> **Nota :** Per poter far funzionare correttamente la comunicazione S7 con alcuni dispositivi SIEMENS di nuova generazione (come S7-1500, S7-1200) è necessario modificare dei parametri della CPU tramite il software SIEMENS TIA Portal prima di poter leggere o scrivere variabili.  Nello specifico, il dispositivo S7 dovrà rispettare i seguenti requisiti: 
>  - opzione ***"Consenti accesso tramite la comunicazione PUT /GET con partner remoti"*** abilitata;
>  - esporre solo Data Blocks (DB) con ***"Accesso non Ottimizzato"*** .

 
Il parametro **"name"** indica il nome che avrà la variabile collegata all'interno del Server OPC UA.
E' possibile leggere e scrivere diverse *Aree di Memoria* dei dispositivi S7, specificando  il parametro **"Area"** :

| Area | Description |
| ------ | ------ |
| MK | Merkers |
| DB | DB |
| PE | Process inputs |
| PA | Process outputs |
| CT | Counters |
| TM | Timers |

Per specificare quali variabili possono essere *solo lette* dal dispositivo S7 e quali possono essere *lette e scritte* da parte del Server OPC UA si utilizza la proprietà ***"writable"*** come booleano.

Per ogni variabile è necessario impostare il tipo del dato S7 tramite il parametro **"dataType"**. 
Sono supportati i tipi ***'Real'**, **'Byte'**, **'Word'**, **'DWord'**, **'Int'**, **'UInt'**, **'DInt'**, **'UDInt'**, **'Bool'**, **'String'*** . 

> ***Nota :*** Rispettare maiuscole e minuscole, i nomi dei tipi sono gli stessi utilizzati da TIA Portal !

Con il parametro **"offset"** viene specificato il Byte dell'indirizzo relativo della variabile.  Questa colonna in TIA Portal è visibile soltanto nei blocchi dati con accesso standard non ottimizzato.
(*es.* variabile S7 di tipo 'Word' con *Offset TIA = 6.0*  dovrà essere specificata con **offset* = 6*).

![enter image description here](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/raw/master/docs/DB_IOT_Tags.PNG)

Per le variabili di tipo ***'Bool'*** è necessario impostare anche il parametro  **"index"** come il Bit dell'indirizzo di memoria della variabile.
(*es.* variabile S7 di tipo 'Bool' con *Offset TIA = 10.1*  dovrà essere specificata con **index* = 1*)

Per le variabili  ***'String'*** è necessario impostare la grandezza in Byte della stringa con la proprietà **"stringLen"**  (max 254 Byte).

## Run the Application

Una volta eseguita la configurazione tramite i passi precedenti è possibile avviare l'applicazione manualmente utilizzando i seguenti comandi da terminale:
```
cd /opt/s7-mqtt-opcuaserver-iot
node app.js
```
Di seguito un esempio di output del programma *s7-mqtt-opcuaserver-iot* in funzione:
```
root@iot2040:~# cd /opt/s7-mqtt-opcuaserver-iot/
root@iot2040:~/s7-mqtt-opcuaserver-iot# node app.js
S7 connection initalized...
Start S7 Pub Polling...
Start OPCUA Server...
The primary Server endpoint URL is opc.tcp://192.168.200.1:4840/ua/node-opcua
OPCUA Server started.
OPCUA Server is now listening on port 4840 (press CTRL+C to stop Server).

```
Dopo l'avvio dell'app è possibile verificare nel log su console l'indirizzo del Server OPC UA come parametro ***"endpoint URL"*** . Copiare l'indirizzo e conservarlo per i nuovi Client OPC UA. 
```
.....'
The primary Server endpoint URL is opc.tcp://192.168.200.1:4840/ua/node-opcua
.....
```

## Manage Startup and Restart with PM2 App

Tramite il tool PM2 può essere gestita automaticamente l'esecuzione di questa applicazione. Con PM2 ad ogni nuovo boot del sistema l'applicazione s7-mqtt-opcuaserver-iot viene avviata e in caso di errori viene automaticamente riavviata per consentire continuità al funzionamento.

Viene fornito lo script ***./install-pm2.sh*** con cui viene automaticamente installato il tool ***PM2*** e sono effettuati i passaggi necessari alla sua configurazione. 
Per poter avviare lo script recarsi sul dispositivo nella cartella dell'app e avviare lo script:
```
cd /opt/s7-mqtt-opcuaserver-iot
chmod +x ./install.sh
./install.sh
```
Di seguito un esempio di ***output*** dell'esecuzione di *./install.sh*
```
root@iot2040:~ cd /opt/s7-mqtt-opcuaserver-iot
root@iot2040:~/s7-mqtt-opcuaserver-iot# chmod +x ./install.sh
root@iot2040:~/s7-mqtt-opcuaserver-iot# ./install-pm2.sh
######################################################################
################# Installing PM2 for Managing Apps! ##################
################## Check your internet connection! ###################
/usr/bin/pm2 -> /usr/lib/node_modules/pm2/bin/pm2
/usr/bin/pm2-dev -> /usr/lib/node_modules/pm2/bin/pm2-dev
/usr/bin/pm2-docker -> /usr/lib/node_modules/pm2/bin/pm2-docker
/usr/bin/pm2-runtime -> /usr/lib/node_modules/pm2/bin/pm2-runtime
/usr/lib
`-- pm2@3.5.0
  `-- shelljs@0.8.3
    `-- glob@7.1.4

npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@^1.2.7 (node_modules/pm2/node_modules/chokidar/node_modules/fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.9: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"ia32"})
######################################################################
################# Creating startup script of PM2 #####################
[PM2] Init System found: upstart
Platform upstart
Template
#!/bin/bash
### BEGIN INIT INFO
# Provides:        pm2
# Required-Start:  $local_fs $remote_fs $network
# Required-Stop:   $local_fs $remote_fs $network
# Default-Start:   2 3 4 5
# Default-Stop:    0 1 6
# Short-Description: PM2 Init script
# Description: PM2 process manager
### END INIT INFO

NAME=pm2
PM2=/usr/lib/node_modules/pm2/bin/pm2
USER=root
DEFAULT=/etc/default/$NAME

export PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin:$PATH
export PM2_HOME="/home/root/.pm2"

...............
...............installation output.....................
...............
............................................................

 System startup links for /etc/init.d/pm2-root already exist.
[PM2] [v] Command successfully executed.
+---------------------------------------+
[PM2] Freeze a process list on reboot via:
$ pm2 save

[PM2] Remove init script via:
$ pm2 unstartup upstart
######################################################################
############### Running application with PM2 System ##################
[PM2] Starting /home/root/s7-mqtt-opcuaserver-iot/app.js in fork_mode (1 instance)
[PM2] Done.
┌─────────────────────┬────┬─────────┬──────┬──────┬────────┬─────────┬────────┬─────┬──────────┬──────┬──────────┐
│ App name            │ id │ version │ mode │ pid  │ status │ restart │ uptime │ cpu │ mem      │ user │ watching │
├─────────────────────┼────┼─────────┼──────┼──────┼────────┼─────────┼────────┼─────┼──────────┼──────┼──────────┤
│ s7-mqtt-opcuaserver │ 0  │ 1.0.0   │ fork │ 2983 │ online │ 0       │ 0s     │ 0%  │ 9.8 MB   │ root │ disabled │
└─────────────────────┴────┴─────────┴──────┴──────┴────────┴─────────┴────────┴─────┴──────────┴──────┴──────────┘
 Use `pm2 show <id|name>` to get more details about an app
######################################################################
##################### Save actual changes in PM2 #####################
[PM2] Saving current process list...
[PM2] Successfully saved in /home/root/.pm2/dump.pm2
######################################################################
###################### INSTALLATION COMPLETED! #######################
######################################################################
##################### REBOOT FOR APPLY CHANGES! ######################
######################################################################

```
Riavviando verranno applicate le modifiche effettuate sopra e l'applicazione sarà in esecuzione automatica. Per verificare lo stato dell'applicazione è possibile utilizzare le funzioni ***PM2***, tra cui:

 ### pm2 list
 Mostra in una tabella tutte le applicazioni gestite da PM2.
```
root@iot2040:~# pm2 list
┌─────────────────────┬────┬─────────┬──────┬──────┬────────┬─────────┬────────┬─────┬──────────┬──────┬──────────┐
│ App name            │ id │ version │ mode │ pid  │ status │ restart │ uptime │ cpu │ mem      │ user │ watching │
├─────────────────────┼────┼─────────┼──────┼──────┼────────┼─────────┼────────┼─────┼──────────┼──────┼──────────┤
│ s7-mqtt-opcuaserver │ 0  │ 1.0.0   │ fork │ 8768 │ online │ 0       │ 4m     │ 43% │ 69.7 MB  │ root │ disabled │
└─────────────────────┴────┴─────────┴──────┴──────┴────────┴─────────┴────────┴─────┴──────────┴──────┴──────────┘

 Use `pm2 show <id|name>` to get more details about an app
```
 ### pm2 logs
 Mostra tutti i file di log delle applicazioni gestite da PM2.
```
root@iot2040:~/s7-mqtt-opcuaserver-iot# pm2 logs
[TAILING] Tailing last 15 lines for [all] processes (change the value with --lines option)
/home/root/.pm2/pm2.log last 15 lines:
PM2        | 2019-05-07T12:49:07: PM2 log: App [s7-mqtt-opcuaserver:0] starting in -fork mode-
PM2        | 2019-05-07T12:49:07: PM2 log: App [s7-mqtt-opcuaserver:0] online
PM2        | 2019-05-08T02:48:27: PM2 log: Stopping app:s7-mqtt-opcuaserver id:0
PM2        | 2019-05-08T02:48:28: PM2 log: pid=8768 msg=failed to kill - retrying in 100ms
PM2        | 2019-05-08T02:48:28: PM2 log: pid=8768 msg=failed to kill - retrying in 100ms
PM2        | 2019-05-08T02:48:28: PM2 log: App [s7-mqtt-opcuaserver:0] exited with code [0] via signal [SIGINT]

/home/root/.pm2/logs/s7-mqtt-opcuaserver-error.log last 15 lines:
/home/root/.pm2/logs/s7-mqtt-opcuaserver-out.log last 15 lines:
0|s7-mqtt-opcuaserver  | S7 connection initalized...
0|s7-mqtt-opcuaserver  | Start S7 Pub Polling...
0|s7-mqtt-opcuaserver  | Start OPCUA Server...
0|s7-mqtt-opcuaserver  | The primary Server endpoint URL is opc.tcp://192.168.200.1:4840/ua/node-opcua
0|s7-mqtt-opcuaserver  | OPCUA Server started.
0|s7-mqtt-opcuaserver  | OPCUA Server is now listening on port 4840 (press CTRL+C to stop Server).
0|s7-mqtt-opcuaserver  | UNSUPPORTED REQUEST !! FindServersOnNetworkRequest

```
Qui è possibile verificare l'indirizzo del Server OPC UA attivo come ***endpoint URL***.

## Connecting to Server OPC UA 

E' possibile utilizzare una qualsiasi applicazione con funzione ***OPC UA Client*** per potersi interfacciare al Server OPC UA creato tramite l'applicazione *s7-mqtt-opcuaserver-iot*.

Di seguito un esempio con GIF di connessione tramite l'applicazione *UAExpert*, scaricabile qui [UAExpert](https://www.unified-automation.com/products/development-tools/uaexpert.html).

![enter image description here](https://gitlab.com/SIMATICIPC/s7-mqtt-opcuaserver-iot/raw/master/docs/UAExpert_Client.gif)

Il ***Certificato CA*** viene prodotto dallo script ***./configure.sh*** , che legge dal dispositivo la configurazione IP e il nome Host. Controllare che il certificato sia riconosciuto correttamente dal client e che i parametri IP, DNS e URI del certificato siano validi rispetto al proprio sistema.

Le variabili OPC UA potranno essere scritte solo se è stato configurato il parametro ***"writable"*** nel file di configurazione ***app-config.json*** come al punto **App Configuration**.

## Installation on other systems

Tutti i *node_modules* sono pre-compliati per IOT2040. per cui non è necessario effettuare il comando npm install per installare i moduli Node.JS. 
Per poter utilizzare l'applicazione con altri dispositivi basterà installare nuovamente tutti i moduli Node.JS necessari, recandosi nella directory dell'app e utilizzando il comando *npm*:
```
cd /opt/s7-mqtt-opcuaserver-iot
rm -rf node_modules
npm install
```
Attendere il completamento dell'installazione.

## Configuration Script

Per potersi identificare in maniera sicura con i propri Client, il *Server OPC UA* implementato utilizza un *Certificato CA* che deve essere specifico per ogni dispositivo. I file necessari dovranno essere generati prima dell'avvio dell'applicazione.
Lo script di configurazione fornito crea automaticamente i  files con i nomi  ***private_key.pem*** e ***certificate.pem***. Questi nomi devono rispettare i valori delle proprietà *"certificateFile"* e *"privateKeyFile"* del file ***app-config.json***

L'applicazione è stata sviluppata con l'utilizzo di *Mosquitto MQTT Broker* (pre-installato in SIMATIC IOT2040) come bus di scambio dati interno.  Per poter utilizzare altri MQTT Broker è necessaria la configurazione con le medesime caratteristiche dei file sopra.
(***mosquitto.conf, aclfile, auth***). 

## Install Git on SIMATIC IOT2040

Il comando ***"git"*** non è installato di default per cui è possibile installare *git* scaricando e compilando direttamente i sorgenti di *git*. Di seguito i comandi da eseguire per l'installazione
```
curl -L https://github.com/git/git/archive/v2.17.0.tar.gz --output git-2.17.0.tar.gz
tar -zxf git-2.17.0.tar.gz
cd git-2.17.0
make prefix=/usr/local all
make prefix=/usr/local install
```
Questa installazione potrebbe durare qualche ora con IOT2040 ma al termine il comando *git* sarà funzionante.

## References

* [node-snap7](https://github.com/mathiask88/node-snap7) - This is a node.js wrapper for snap7. Snap7 is an open source, 32/64 bit, multi-platform Ethernet communication suite for interfacing natively with Siemens S7 PLCs.
* [asyncawait](https://github.com/yortus/asyncawait) - asyncawait addresses the problem of callback hell in Node.js because of Node.JS V 6.X 
* [node-opcua](https://github.com/node-opcua/node-opcua) - an implementation of a OPC UA stack fully written in javascript and nodejs
* [PM2](https://github.com/Unitech/pm2) - PM2 is a production process manager for Node.js applications with a built-in load balancer. It allows you to keep applications alive forever, to reload them without downtime and to facilitate common system admin tasks.
* [mqtt](https://github.com/mqttjs/MQTT.js) - MQTT.js is a client library for the [MQTT](http://mqtt.org/) protocol, written in JavaScript for node.js and the browser.


## Release History

-   1.0.0
    -   The first proper release

## License

Distributed under the MIT License. See  `LICENSE`  for more information.

## Contributing

1.  Fork it ([https://github.com/yourname/yourproject/fork](https://github.com/yourname/yourproject/fork))
2.  Create your feature branch (`git checkout -b feature/fooBar`)
3.  Commit your changes (`git commit -am 'Add some fooBar'`)
4.  Push to the branch (`git push origin feature/fooBar`)
5.  Create a new Pull Request

## Contact

SIMATICIPC –  [SIMATICIPC@GitLab](https://gitlab.com/SIMATICIPC/)
