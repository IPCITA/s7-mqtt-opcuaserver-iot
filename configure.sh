#!/bin/bash
######################################################################
################ Configuration script for opcua-client################ 
######################################################################

echo "######################################################################"
echo "############# Copying Mosquitto MQTT configuration files #############"
ME=$(whoami)
sed "s/###USERAPP/user ${ME}/g" ./mqtt/app.conf > ./mqtt/mosquitto.conf
echo "your user is ${ME}""
cp mqtt/mosquitto.conf /etc/mosquitto/mosquitto.conf
cp mqtt/aclfile /etc/mosquitto/aclfile
cp mqtt/auth /etc/mosquitto/auth
echo "mqtt files copied successfully."

echo "######################################################################"
echo "####################### Generate CA Certificates #####################"
echo "############## Creating your new certificates. Wait! #################"

#####GENERATE SELF SIGNED CERTIFICATES FOR OPCUA SERVER
#READ HOSTNAME AND COPY CONFIG TEMPLATE IN A NEW CONFIG FILE WITH NEW HOSTNAME

HOSTNAME=$(hostname)
sed "s/#HOST/${HOSTNAME}/g" ./cert/openssl.cnf > ./cert/myopenssl.cnf
echo "your hostname is ${HOSTNAME}"

#####
#FOR EACH INTERFACES READ IP AND REPLACE IN  CONFIG FILE
INTERFACES=$(ip link | awk -F: '$0 !~ "lo|vir|wl|^[^0-9]"{print $2;getline}')
i=1
for n in $INTERFACES
do
	IPS=$(ifconfig | grep -A 1 ${n} | tail -1 | cut -d ':' -f 2 | cut -d ' ' -f 1)
	#CHECK IF IS A VALID IP
	TESTSEQ="."
	if [[ $IPS =~ $TESTSEQ ]];
	then
    		sed  -i "s/#IP${i}/IP.${i}   = ${IPS}/g" ./cert/myopenssl.cnf
		echo "Find ${n} with IP Address ${IPS}"
	else
    		echo "No valid IP Address for ${n}"
	fi
    	
    	i=$((i+1))
done

echo "OpenSSL config generated."

#GENERATE KEY AND CERT
openssl req -x509 -days 365 -nodes -newkey rsa:2048 -keyout ./cert/private_key.pem \
-out ./cert/certificate.pem -config ./cert/myopenssl.cnf

echo "######################################################################"
echo "###################### CONFIGURATION COMPLETED! ######################"
echo "######################################################################"
echo "### SET AUTOSTART OF MOSQUITTO SERVICE USING iot2000setup COMMAND. ###"
echo "############ GO TO Software -> Manage Autostart Options AND ##########"
echo "############ ENABLE Autostart Mosquitto Broker.'            ##########"
echo "######################################################################"
echo "##### AFTER SETTINGS ABOVE REBOOT YOUR SYSTEM TO APPLY SETTINGS! #####"
echo "############### RUN install.sh AFTER REBOOT FOR SET       ############"
echo "############### AUTOSTART s7-mqtt-opcuaserver-iot AT BOOT.############"
echo "######################################################################" 
